#!/bin/bash

for dep in nicinfo \
	   sort \
	   date \
	   tee;
do
  { hash "${dep}" 2>/dev/null; } \
  || \
  { echo -e "\n[FAIL] Required dependency (${dep}) not installed; exiting." 1>&2;
    exit 1; };
done;

declare -A cidr_array;

while read -r key values; do
  if [[ "$key" =~ CIDRs: ]]; then
    IFS=$',';
    for cidr in ${values//[[:space:]]}; do
	cidr_array["$cidr"]="${cidr}";
    done;
    unset IFS;
  fi;
done < <(nicinfo -Vt entityhandle 7ESG 2>&1);

if [[ "${#cidr_array[@]}" != "0" ]]; then
  for cidr in "${cidr_array[@]}"; do
      echo "${cidr}";
  done \
  | sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n \
  | tee -a "afnets.$(date --universal --iso-8601=seconds).log";
else
  echo -e "\n[FAIL] Script failed to gather any results; exiting." 1>&2;
  exit 1;
fi;
