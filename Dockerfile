FROM ruby:latest
RUN { echo -e "\n[INFO] Upgrading system packages and installing required utilities." && \
      apt-get update -y && \
      apt-get upgrade -y && \
      apt-get install -y bash coreutils && \
      gem install nicinfo; } \
    || \
    { echo -e "\n[FAIL] Failed to install required dependencies; exiting."; exit 1; };
WORKDIR /workdir
ADD  ./afnets.sh /usr/bin/afnets.sh 
RUN { echo -e "\n[INFO] Placing afnets.sh script in path, and testing." && \
      chmod +x /usr/bin/afnets.sh && \
      afnets.sh; } \
    || \
    { echo -e "\n[FAIL] Script testing failed; exiting."; exit 1; };
ENTRYPOINT afnets.sh
