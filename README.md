# afnets-parser

Parses, dedupes, and sorts the current list of AF networks listed with ARIN.

## Background

ARIN is the American Registry for Internet Numbers. It is one of a handful of regional internet registries responsible for handing out public IP blocks on behalf of the Internet Assigned Numbers Authority (IANA), which is a standards organization entrusted with overseeing global IP address allocation. An IANA-like authority dates all the way back to the ARPANET days when the internet was still a DoD-only network of networks. As it transitioned into what is now the internet, the need to control the allocation of IP space did not go away, and as a result, the U.S. government opted to fund an entity responsible for doing so. As a matter of fact, this activity is still funded by the U.S. government to this day.

For a more detailed description of IANA's history, please visit this informational webpage:

- https://www.internetsociety.org/ianatimeline/

Because every single IP address used on the internet is supposed to be reserved and issued by IANA, (through its regional internet registries like ARIN), any company, person, or government agency that wishes to reserve public IP address space, must do so through the regional internet registry that services their geographic region. ARIN, in particular, services the United States; therefore, any requests for reserved IP spaces made for U.S. Air Force enterprise networks, are made through ARIN.

ARIN maintains a list of organizations and their IP spaces on a public-facing website. Each organization has a unique organization ID, and some organizations maintain sub-IDs. The organization in charge of Air Force enterprise networks has wisely centralized all its subordinate units under a single organization ID:

- 7ESG

This URL shows some details about the organization, and subordinate organizations assigned to it:

- https://whois.arin.net/rest/org/7ESG.html

While it is useful to be able to manually access and review all these sub-organizations, and their allocated IP ranges, it is much more useful to be able to do this programmatically (i.e. through automation).

Luckily, someone has already created a tool that does this precise thing:

- https://github.com/arineng/nicinfo

This tool is packaged as a Ruby Gem, and provides a user the ability to pass the organization's ID to it, and in return, the user receives a detailed list of sub-organization names, their IP spaces, and a lot of other information regarding the organization.

This is helpful, but in order to sort out just the information required, additional tools are needed, and some minor scripting is required. This scripting is included in this repository (afnets.sh). It uses `nicinfo` and some other tools to parse out just the IP addresses, cutting out information users are not concerned with.

Since this script requires a user to install `nicinfo` and the other tools it relies upon, this process has been made even easier. This repository also includes a Dockerfile that can be used to create a Docker container with all the tools, (including the script), pre-installed and ready to use.

## Using The Docker Container

All the user has to do to use this container is have the `docker` utility installed, run a simple `docker` command, and the user will have a list of all Air Force enterprise network IP space returned to them in a few seconds in their terminal.

This list is sorted and deduplicated by the `afnets.sh` script running inside the container, and only includes the IP addresses (in CIDR notation), stripping out all other data about the organizations. This clean IP-only list can then be fed programmatically into another tool that automatically creates firewall rules based on the parsed IP addresses, if the user decides to use it for that purpose.

The IP list written to the terminal will be written to a timestamped file in the current working directory, inside the container.
If that directory is mounted from the host machine running the Docker container, it will also be written to the host.

This is the Docker command used to run the container on host machine with Docker installed:

```
docker run \
  --rm \
  -it \
  -v $PWD:/workdir \
  registry.gitlab.com/${repo_org_name_here}/afnet-parser/image:latest;
```

Note: ensure the variable in the registry URL is replaced with the Gitlab organization name.

## Semantic Releases

When running container images from this repository, feel free to replace "latest" with a specific version number found in the tags of this repository. The container images, (and this repository), are version controlled using the specifications of Semantic Versioning. The `semantic-release` tool is used to tag specific commits on the master branch with a version number, place a version file in the repository after each commit, and update a change log with release information after any commit that triggers a version bump.

## Contributing To This Tool

First, clone this repository:

```
git clone "${repo-clone-url-here}"
cd afnets-parser;
```

To build a fresh container image, you'll need to do a quick Docker build:

```
docker build --no-cache -t afnets-parser .
```

Once built, run like so to get a list of current ARIN-registered AF enterprise networks ranges:

```
docker run -it --rm -v $PWD:/workdir afnets-parser
```

This will return a sorted, deduped list of subnets in CIDR notation,
and this same data will be written to a timestamped file in the current working directory.

Mounting the current working directory will allow changes to files to appear on the host.

### Modifying the Script

Using your text editor of choice to modify `afnets.sh`.

The script is linted with `shellcheck` in the CI/CD pipeline. If you'd like to pre-screen it locally:

```
docker run --rm -it -v $PWD:/workdir koalaman/shellcheck-alpine /bin/sh -c 'shellcheck -f json /workdir/afnets.sh'
```

If everything is correct, this command should return an empty json object (i.e. "[]").

If not, it should return errors that need to be corrected to pass linting.

Refer to the documentation for `shellcheck` for information on lint rule exclusions.

### Updating The README

The `README.md` is spellchecked and markdown linted in the CI/CD pipeline. If you'd like to pre-screen it locally:

```
# On the Docker host
docker run \
  --rm \
  -it \
  -v $PWD:/workdir \
  -w /workdir \
  registry.gitlab.com/dreamer-labs/repoman/dl-lint-docs:dc159d5d-lint-docs \
  /bin/bash;
# In the Docker container
pyspelling --spellchecker hunspell;
```

```
# On the Docker host
docker run \
  --rm \
  -it \
  -v $PWD:/workdir \
  -w /workdir \
  registry.gitlab.com/dreamer-labs/repoman/dl-lint-docs:dc159d5d-lint-docs \
  /bin/bash;
# In the Docker container
mdl README.md;
```

Refer to the documentation of the individual tools for information on linting and spelling exclusions.
