## [1.0.1](https://gitlab.com/codrcodz/afnets-parser/compare/v1.0.0...v1.0.1) (2020-09-03)


### Bug Fixes

* Adds fix to allow image release ([23e14a6](https://gitlab.com/codrcodz/afnets-parser/commit/23e14a6))

# 1.0.0 (2020-09-03)


### Features

* Add better error handling ([4461558](https://gitlab.com/codrcodz/afnets-parser/commit/4461558))
* Init repository ([5c7e700](https://gitlab.com/codrcodz/afnets-parser/commit/5c7e700))
* Update sorting algorithm to better support ipv4 ([6396662](https://gitlab.com/codrcodz/afnets-parser/commit/6396662))
